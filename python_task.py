# -*- coding: cp936 -*-
#创建多个线程对多个文件的读取，处理，写入excel的多个sheet
from pyExcelerator import *
import string
import thread
import threading

w = Workbook()
font0 = Font()

font0.bold = True
style0 = XFStyle()
style0.font = font0
strbuf = ['test1.txt','test2.txt','test3.txt'] # An array of storage file names

class ThreadAddSheet(threading.Thread):
        def __init__(self,file_name): #线程构造函数
                threading.Thread.__init__(self)
                self.file_name = file_name
        def run(self): #具体的线程运行代码
                ws = w.add_sheet(self.file_name)
                f = file(self.file_name)
                # if no mode is specified, 'r'ead mode is assumed by default
                num = 0
                sum_num = 0
                while True:
                    line = f.readline()
                    if len(line) == 0: 
                        break
                    strip = line.split()
                    for i in range(0,len(strip)):
                        flag = 0
                        if strip[i].isdigit() and strip[i-1] == 'is' and strip[i+1] == 'ms':     
                            ws.write(num,0,strip[i],style0)
                            sum_num += string.atoi(strip[i])
                            num += 1
                ws.write(num,0,'average:',style0)               
                ws.write(num + 1,0,sum_num/num,style0)
                # Notice comma to avoid automatic newline added by Python
                w.save('mini.xls')  

threads = len(strbuf) #当读取的文件少时，一个文件对应一个线程，文件多时，要把文件平均分给少于文件数的线程               
for index in range(threads):
        thread = ThreadAddSheet(strbuf[index])
        thread.start() #启动线程
